// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Flocker.h"
#include "SpawnActor.generated.h"


UCLASS()
class PARKINGLOTWARS_API AManager : public AActor
{
	GENERATED_BODY()
private:
	AFlocker** flockers;
	FVector pos, target;
public:
	// Sets default values for this actor's properties
	AManager();

	UPROPERTY(EditAnywhere, Category = "init vars")
		int32 numAgents;

	UPROPERTY(EditAnywhere, Category = "init vars")
		TSubclassOf<AFlocker> spawnObject;

	UPROPERTY(EditAnywhere, Category = "init vars")
		float spawnRadius;

	UPROPERTY(EditAnywhere, Category = "init vars")
		float distThreshold;

	float RunningTime;

	UFUNCTION(BlueprintCallable, Category = "manager_func")
	void SetTarget(FVector v);

	void manage(float DeltaTime);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;



};
