// Fill out your copyright notice in the Description page of Project Settings.

#include "ParkingLotWars.h"
#include "ClientComponent.h"

#include "Client.h"


// Sets default values for this component's properties
UClientComponent::UClientComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	client.cp = this;
	// ...
}


// Called when the game starts
void UClientComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UClientComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}


void UClientComponent::SendWaypoint(FVector v) {
	ClientData cd;
	cd.data = 'p';
	cd.target = vec3{ v.X, v.Y, v.Z };
	client.sendData(cd);
}

void UClientComponent::ReceivePosition_Implementation(FVector v) {
	UE_LOG(LogTemp, Warning, TEXT("I'm running!"));
}