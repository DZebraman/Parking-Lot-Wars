// Fill out your copyright notice in the Description page of Project Settings.

#include "ParkingLotWars.h"
#include "Flocker.h"


// Sets default values
AFlocker::AFlocker()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFlocker::BeginPlay()
{
	Super::BeginPlay();
	pos = GetActorLocation();
}

void AFlocker::setCentroid(FVector _centroid) {
	centroid = _centroid;
}
void AFlocker::setAvoid(AFlocker* _toAvoid) {
	toAvoid = _toAvoid;
	avoidPos = toAvoid->GetActorLocation();
}

// Called every frame
void AFlocker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	vel.Normalize();

	FVector toTarget = centroid - pos;
	toTarget.Normalize();

	DrawDebugLine(
		GetWorld(),
		pos,
		pos + toTarget * 20,
		FColor::Magenta,
		false,
		-1.f,
		0,
		2.f
		);

	vel = FMath::Lerp(vel, toTarget, cohesion);

	if (toAvoid) {
		FVector avoid = -(avoidPos - pos);
		avoid.Normalize();
		vel = FMath::Lerp(vel, avoid, separation);

		DrawDebugLine(
			GetWorld(),
			pos,
			pos + avoid * 20,
			FColor::Red,
			false,
			-1.f,
			0,
			2.f
			);

	}

	vel.Normalize();
	vel *= maxSpeed * DeltaTime;
	DrawDebugLine(
		GetWorld(),
		pos,
		pos + vel * 5,
		FColor::Green,
		false,
		-1.f,
		0,
		2.f
		);

	pos += vel;



	toAvoid = nullptr;

	SetActorLocation(pos);
}

