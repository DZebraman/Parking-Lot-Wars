// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Flocker.generated.h"

UCLASS()
class PARKINGLOTWARS_API AFlocker : public AActor
{
	GENERATED_BODY()
private:
	FVector centroid, pos, vel, avoidPos;
	AFlocker* toAvoid;
public:
	// Sets default values for this actor's properties
	AFlocker();

	UPROPERTY(EditAnywhere, Category = "flocking vars")
		float maxSpeed;

	UPROPERTY(EditAnywhere, Category = "flocking vars")
		float separation;

	UPROPERTY(EditAnywhere, Category = "flocking vars")
		float cohesion;

	void setCentroid(FVector _centroid);
	void setAvoid(AFlocker* _toAvoid);
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
};
