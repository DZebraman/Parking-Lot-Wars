// Fill out your copyright notice in the Description page of Project Settings.

#include "ParkingLotWars.h"
#include "Client.h"
#include "ClientComponent.h"

Client::Client()
{
}

Client::~Client()
{
}

void Client::clientSetupUDP() {
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
	{
		printf("socket() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}

	memset(&si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(PORT);
	si_other.sin_addr.S_un.S_addr = inet_addr(SERVER);
}

int Client::run() {
	slen = sizeof(si_other);
	clientSetupUDP();

	//start communication
	clientLogic();

	closesocket(s);
	WSACleanup();

	return 0;
}

void Client::clientLogic() {
	char buf[BUFLEN];
	std::string message = "";

	username = "\0";
	do {
		username = usernames[rand() % numUsers];
		if (message[0]) {
			std::string userMess = "\\U:/" + message;
			sendUDP(userMess.c_str(), userMess.length(), &si_other, &slen, s);
			recvUDP(buf, &si_other, &slen, s);
			if (buf[0] == 'a') username = message;
			else if (buf[0] == 't') continue;
			else if (buf[0] == 'c') return;
		}
	} while (!username[0]);

	std::thread incoming([this] {
		char buf[BUFLEN];
		//printf("listening at address %s port %u", inet_ntoa(si_other->sin_addr), ntohs(si_other->sin_port));
		while (true) {
			recvUDP(buf, &si_other, &slen, s);
			ServerData sd = *(ServerData*)buf;
			switch (sd.data) {
			case 'p':
				cp->ReceivePosition(FVector(sd.position.x, sd.position.y, sd.position.z));
				break;
			}
		}
	});

	incoming.detach();
	while (true);
}

void Client::sendData(ClientData cd) {
	sendUDP((char*)&cd, sizeof(cd), &si_other, &slen, s);
}

void Client::sendUDP(const char* message, int mlen, struct sockaddr_in* si_other, int* slen, SOCKET s) {
	if (sendto(s, message, mlen, 0, (struct sockaddr *) si_other, *slen) == SOCKET_ERROR)
	{
		printf("sendto() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
}

int Client::recvUDP(char* buf, struct sockaddr_in* si_other, int* slen, SOCKET s) {
	memset(buf, '\0', BUFLEN);
	int len;
	if ((len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) si_other, slen)) == SOCKET_ERROR)
	{
		printf("recvfrom() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	return len;
}