// Fill out your copyright notice in the Description page of Project Settings.

#include "ParkingLotWars.h"
#include "SpawnActor.h"




// Sets default values
AManager::AManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AManager::BeginPlay()
{
	Super::BeginPlay();
	flockers = new AFlocker*[numAgents];
	pos = GetActorLocation();

	for (int i = 0; i < numAgents; i++) {
		FVector tempLocation = pos + FMath::VRand() * spawnRadius;
		flockers[i] = (AFlocker*)GetWorld()->SpawnActor(spawnObject, &tempLocation);
		flockers[i]->setCentroid(pos);
	}

	distThreshold *= distThreshold;
}

void AManager::SetTarget(FVector v) {
	target = v;
}

void AManager::manage(float DeltaTime) {
	//pos += FVector(0, FMath::Sin(RunningTime), FMath::Cos(RunningTime)) * 60;'
	pos = FMath::Lerp(pos, target, DeltaTime);

	SetActorLocation(pos);

	int* closest = new int[numAgents];
	for (int i = 0; i < numAgents; i++) {
		closest[i] = -1;
	}
	for (int i = 0; i < numAgents; i++) {
		flockers[i]->setCentroid(pos);
		float tempClosest = distThreshold;
		for (int k = i + 1; k < numAgents; k++) {
			float tempDist = FVector::DistSquared(flockers[i]->GetActorLocation(), flockers[k]->GetActorLocation());
			if (tempDist < tempClosest) {
				closest[i] = k;
				closest[k] = i;

				/*DrawDebugLine(
				GetWorld(),
				flockers[i]->GetActorLocation(),
				flockers[k]->GetActorLocation(),
				FColor::Red,
				false,
				-1.f,
				0,
				2.f
				);*/
			}
		}
		if (closest[i] > -1) {
			flockers[i]->setAvoid(flockers[closest[i]]);
		}
	}
}

// Called every frame
void AManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	manage(DeltaTime);

	RunningTime += DeltaTime;
}

