// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SpawnActor.h"
#include "GameFramework/Actor.h"
#include "Buttmaster.generated.h"

UCLASS()
class PARKINGLOTWARS_API AButtmaster : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AButtmaster();

	UPROPERTY(EditAnywhere, Category = "init vars")
		TSubclassOf<AFlocker>  spawnObject;



	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	
};
