// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AllowWindowsPlatformTypes.h"
#include <winsock2.h>
#include "HideWindowsPlatformTypes.h"
#include <thread>
#include <vector>

#define SERVER "127.0.0.1"  //ip address of udp server

#define BUFLEN 512  //Max length of buffer
#define PORT 8888   //The port on which to listen for incoming data

struct vec3 { float x, y, z; };
struct flockSplit { bool split; uint32_t other; };

class UClientComponent;

struct ClientData {
	char data;
	union {
		vec3 target;
		flockSplit split;
	};
};

struct ServerData {
	char data;
	union {
		vec3 position;
	};
};

/**
 * 
 */
class PARKINGLOTWARS_API Client
{
public:
	Client();
	~Client();

	void clientSetupUDP();
	int run();
	void clientLogic();

	static void sendUDP(const char* message, int mlen, struct sockaddr_in* si_other, int* slen, SOCKET s);
	static int recvUDP(char* buf, struct sockaddr_in* si_other, int* slen, SOCKET s);

	void sendData(ClientData cd);

	UClientComponent* cp;

private:
	SOCKET s;
	struct sockaddr_in si_other;
	WSADATA wsa;
	int slen;

	std::string usernames[5] = { "bob", "steve", "larry", "dave", "butts" }, username;
	int numUsers = 5;
};