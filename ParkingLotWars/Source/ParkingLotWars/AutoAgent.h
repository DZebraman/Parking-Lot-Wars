// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include "GameFramework/Actor.h"
#include "AutoAgent.generated.h"

UCLASS()
class PARKINGLOTWARS_API AAutoAgent : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "flocking vars")
		FVector pos;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "flocking vars")
		FVector vel;
	FVector acc, fwd;
public:
	// Sets default values for this actor's properties
	AAutoAgent();

	//UPROPERY(EditAnywhere, Category = "init vars");
	UPROPERTY(EditAnywhere, Category = "init var")
		float maxSpeed;


	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual FVector seek(FVector target);
	virtual FVector flee(FVector target);
	virtual FVector pursue(FVector target, FVector targetPrev);
	virtual FVector evade(FVector target, FVector targetPrev);
	virtual FVector arrive(FVector target);
	virtual FVector getPos();
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
};