// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Client.h"

#include "Components/ActorComponent.h"
#include "ClientComponent.generated.h"

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PARKINGLOTWARS_API UClientComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UClientComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, Category = "client_func")
		void SendWaypoint(FVector v);

	UFUNCTION(BlueprintNativeEvent, Category = "server_event", meta = (DisplayName = "Server ~ Receive Position"))
		void ReceivePosition(FVector v);

private:
	Client client;
		
	
};
