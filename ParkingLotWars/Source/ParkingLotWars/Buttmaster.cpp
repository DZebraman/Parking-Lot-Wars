// Fill out your copyright notice in the Description page of Project Settings.

#include "ParkingLotWars.h"
#include "Buttmaster.h"


// Sets default values
AButtmaster::AButtmaster()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AButtmaster::BeginPlay()
{
	Super::BeginPlay();
	AFlocker* spawned = (AFlocker*)GetWorld()->SpawnActor(spawnObject, &FVector::ZeroVector);
}

// Called every frame
void AButtmaster::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

